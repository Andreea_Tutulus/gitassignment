package ro.ase.csie.cts.g1084.tema2;

import java.util.ArrayList;

public class TestGit {

	public static void main(String[] args) {
		System.out.println("Hello Git ! Denumirea proiectului de licenta este [Dezvoltarea unei solutii informatice pentru gestiunea activitatii unui centru medical]");
		ArrayList<Integer> programari=new ArrayList<Integer>();
		programari.add(4);
		programari.add(1);
		programari.add(6);
		Medic medic=new Medic("Popescu",programari);
		System.out.println("Nr mediu de programari: "+Math.round(medic.nrMediuProgramari()));
	}

}
