package ro.ase.csie.cts.g1084.tema2;

import java.util.ArrayList;

public class Medic implements Ocupabil{
	
	String nume;
	ArrayList<Integer> programariPeSaptamana=new ArrayList<>();
	
	public Medic(String nume, ArrayList<Integer> programari) {
		super();
		this.nume = nume;
		this.programariPeSaptamana = programari;
	}

	@Override
	public float nrMediuProgramari() {
		if(programariPeSaptamana.size()!=0) {
			int suma=0;
			for(int programariPeZi:programariPeSaptamana) {
				suma+=programariPeZi;
			}
			return suma/programariPeSaptamana.size();
		}
		return 0;
	}

}
